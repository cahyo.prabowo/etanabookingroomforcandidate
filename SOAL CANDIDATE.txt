Mohon Untuk Mengerjakan Soal Ini Dengan Teliti 


	PT ABC Meminta Pihak System Developer Untuk Membuat Aplikasi Untuk Manajemen Permintaan Booking Ruangan Meeting ,
dan kandidat diminta untuk meneruskan aplikasi yang sudah ada sehingga aplikasi dapat berjalan dengan baik , untuk detail fitur yang diminta sebagai berikut 


1. Role di Aplikasi ada Sebagai Requestor Dan Ada Sebagai Approver
2. Buat Fungsi Login saat ini login masih melalui link https://baseur/AccessWorkspace?Odec=E0179 dimana parameter Odec itu diisi NIP setiap karyawan yang ingin mengakses aplikasi 
3. Tampilkan Room Facility Sesuai Dengan Ruangan Meeting yang ada 
4. Buat Fungsi Penyimpanan Booking Dengan syarat 
   -> Saat Request Booking di Hari dan Ruangan Yang sama Available Session tidak bisa di booking lebih dari satu requestor
      kecuali pengajuan requestnya di reject 
   -> Available Session bisa di pilih multi 
   -> Untuk Kolom Total Participant , Available Session , Internal External dan Additional INformation di buat Mandatory 
   
5. Tampilkan List 
   My Booking Room = List Booking Yang di Request 
   My History Booking = List Booking Yang di Request namun Pengajuannya Sudah Lewat 
   Pending Approval = List Pengajuan Request Booking yang harus di approve / reject 



